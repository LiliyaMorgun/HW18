window.onload = () => {

	let button1 = document.querySelector("input");
	
	button1.onclick = () =>{
		
		let diameter = document.createElement("input");
		let butDraw = document.createElement("input");
		diameter.placeholder ="Enter diameter of circle (px)";
		
				
		butDraw.type = "button";
		butDraw.value = "Draw";
		button1.replaceWith(diameter);
		diameter.after(butDraw);
		
		butDraw.onclick = () =>{

           for(i=0;i<100;i++){
                let circle = document.createElement("div");
                circle.classList.add("circle");
                circle.style.width = diameter.value + "px";
                circle.style.height = diameter.value + "px"; 
				circle.style.backgroundColor = `hsl(${Math.floor(Math.random() *360)}, 50%, 50%)`;
				circle.style.borderColor = `hsl(${Math.floor(Math.random() *360)}, 50%, 50%)`;             
			    document.body.append(circle);
				butDraw.replaceWith(circle);
				diameter.replaceWith(circle);
			    circle.onclick = (j) =>{
				j.target.remove()
			}              
            }
		}		
	}
}
